GD-gettext Converter
====================

Converts GD locale files to GNU gettext and gettext back into GD locales.


Usage
-----

`gdgettext.py [-h] [-o PATH] [-v] [-w] [-d] [-c] INPUT`

### Parameters:

`-h` or `--help`:
Display usage help.

#### Positional parameters:

`INPUT`:
Directory containing GD locales or a .po file.

#### Optional parameters:

`-o` or `--output`:
Directory to output converted file(s); default is CWD.

`-v` or `--verbose`:
Print detailed conversion progress.

`-w` or `--overwrite`:
Overwrite existing file(s) without asking.

`-d` or `--keep-duplicates`:
When converting GD to gettext, will keep all duplicated strings
in a file; default behavior is to create a single entry from all
the duplicates and store the occurences alongside it.

`-c` or `--no-autocontext`:
When converting GD to gettext, won't try to guess the entry
context automatically.


How to run (for dummies)
------------------------

### Requirements:

- Python 2.7.x
- polib

### On Windows:

1. Go to https://www.python.org/downloads/windows/ and get the Python 2.7.x installer then install it. **Make sure you enable the "Add to PATH" option during the installation.**

    *x = whatever version is there*

    *You may already have Python 2.7.x installed. Check by opening the command prompt and typing `python --version`*

2. Open the command prompt as Administrator (Right click -> Run as Administrator) and install polib with the command: `pip install polib`

3. Type the command `python <whatever_directory_you_placed_gdgettext>\gdgettext.py --help`
