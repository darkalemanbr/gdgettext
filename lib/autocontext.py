# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at http://mozilla.org/MPL/2.0/.

import os
import io


def guess_context(fo):
    with io.open(fo.fileno(), mode="rt", encoding="utf8", closefd=False) as f:
        filename = os.path.basename(fo.name)
        subcontexts = []

        if filename[0:3] == "bq_":
            subcontexts.append("quest log")
            subcontexts.append("bounty")

            if filename[3:5] == "bl":
                subcontexts.append("black legion")
            elif filename[3:5] == "dc":
                subcontexts.append("darius cronley")
            elif filename[3:8] == "exile":
                subcontexts.append("devil's crossing exiles")
            elif filename[3:5] == "hs":
                subcontexts.append("homestead")
            elif filename[3:5] == "kc":
                subcontexts.append("kymon's chosen")
            elif filename[3:6] == "odv":
                subcontexts.append("death's vigil")
            elif filename[3:5] == "ro":
                subcontexts.append("rovers")
        elif filename[0:3] in ("sq_", "mq_"):
            subcontexts.append("quest log")
            subcontexts.append(
                "{} quest".format("main" if filename[0:1] == "m" else "side")
            )

            pos = fo.tell()
            fo.seek(0, os.SEEK_SET)

            for i in xrange(2):
                subcontexts.append(               #
                    f.readline().strip().lower()  # quest location and title
                )                                 #

            fo.seek(pos, os.SEEK_SET)
        elif filename[0:4] == "npc_" or filename[0:7] == "object_":
            subcontexts.append("{} conversation".format(
                "npc" if filename[0:1] == "n" else "object"
            ))

            pos = fo.tell()
            fo.seek(0, os.SEEK_SET)

            subcontexts.append(f.readline().strip().lower())  # npc/object name

            fo.seek(pos, os.SEEK_SET)
        elif filename[0:5] == "tags_":
            root, ext = os.path.splitext(filename)
            pre, sep, post = root.partition("_")

            subcontexts.append(post.replace("_", " "))

        return " / ".join(subcontexts)
