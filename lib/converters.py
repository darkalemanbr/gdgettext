# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at http://mozilla.org/MPL/2.0/.

""" Functions to convert between GD locales and gettext. """

import os
import glob
import ConfigParser
import itertools
import polib
import time
import io
import util
import autocontext


def gd_to_gettext(indir, outfile, verbose=False, overwrite=False,
                  duplicates=False, autoctxt=True):
    """
    Converts GD locales (``.txt`` files) in the folder `indir`, where a valid
    ``language.def`` must also be present, to a gettext template file
    (``grimdawn.pot``).

    :param indir: Directory of the GD locales
    :type indir: str

    :param outfile: Output directory of the ``grimdawn.pot`` file
    :type outfile: str

    :param verbose: :keyword:print progress info
    :type verbose: bool

    :param overwrite: Overwrite ``grimdawn.pot`` file if it exists, without
                      asking.
    :type overwrite: bool

    :param duplicate: Create an entry for every string in a GD locale file, no
                      matter what. Default behavior is to create a single entry
                      of all the duplicates and append the occurrences to the
                      entry's references. Please note that this behavior is
                      only per-file, as the same string in other files will
                      still produce a new entry.
    :type duplicate: bool

    :param autoctxt: Automatically guess the entry's context from its file's
                     name and content.
    :type autoctxt: bool

    :rtype: None
    """

    conf = ConfigParser.ConfigParser()

    langdef = os.path.join(indir, "language.def")
    with io.open(langdef, encoding="utf8") as f:
        if verbose:
            print "Parsing \"{}\"".format(langdef)

        conf.readfp(util.FakeSection(f))

    if not conf.has_option("DEFAULT", "language") \
            or not conf.has_option("DEFAULT", "author"):
        raise Exception(
            "language.def files must have the \"language\" and \"author\" "
            "options set"
        )

    if not overwrite and os.path.isfile(outfile):
        if verbose:
            print "File \"{}\" already exists".format(outfile)

        if not _ask_overwrite(outfile):
            return

    po = polib.POFile()
    po.metadata = {
        "POT-Creation-Date": time.strftime("%Y-%m-%d %H:%m%z"),
        "MIME-Version": "1.0",
        "Content-Type": "text/plain; charset=utf8",
        "Content-Transfer-Encoding": "8bit",
        "X-gdgettext": ""
    }

    line_count = {}

    filelist = glob.glob(os.path.join(indir, "*.txt"))
    for filename in filelist:
        filename_base = os.path.basename(filename)
        line_count[filename_base] = 0
        file_entries = []
        last_comment = None

        if filename_base[0:5] == "tags_":
            parsemode = 1
        else:
            parsemode = 0

        if verbose:
            print "Parsing \"{}\"".format(filename_base)

        with open(filename, mode="rb") as fo:
            f = io.open(fo.fileno(), mode="rt", encoding="utf8", closefd=False)

            for line in f:
                line_count[filename_base] += 1

                trimmed_line = line.strip(" \r\n")
                trimmed_len = len(trimmed_line)

                if trimmed_len < 1:
                    continue

                if parsemode == 1:  # tags_
                    if line.lstrip()[0:1] == "#":  # comment
                        comment_str = line.strip(" #\r\n")

                        if (len(comment_str) > 0):
                            last_comment = comment_str

                        continue

                msgid = line.rstrip("\r\n")
                msgline = line_count[filename_base]

                if not duplicates and parsemode != 1:
                    existing_entry = _find_duplicate(file_entries, msgid)

                    if existing_entry is not None:
                        existing_entry.occurrences.append(
                            (filename_base, msgline)
                        )
                        continue

                entry_args = {
                    "msgid": msgid,
                    "occurrences": [(filename_base, msgline)]
                }

                if last_comment is not None:
                    entry_args["comment"] = last_comment

                if autoctxt:
                    entry_args["msgctxt"] = autocontext.guess_context(fo)

                entry = polib.POEntry(**entry_args)

                file_entries.append(entry)

        for e in file_entries:
            po.append(e)

        if verbose:
            print "{} entries parsed".format(len(file_entries))

    po.metadata["X-gdgettext-Line-Counts"] = " ".join(
        ["{}:{}".format(k, v) for (k, v) in line_count.iteritems()]
    )

    if verbose:
        print "Writing to \"{}\"".format(outfile)

    po.save(outfile)

    if verbose:
        print "Done! *shake shake shake* *zip*"


def gettext_to_gd(infile, outdir, verbose=False, overwrite=False):
    """
    Converts the gdgettext-generated gettext ``.po`` file `infile` back into
    GD locales.

    :param infile: Path to a ``.po`` file initially generated by gdgettext
    :type infile: str

    :param outdir: Output directory of GD locale files
    :type outdir: str

    :param verbose: :keyword:print progress info
    :type verbose: bool

    :param overwrite: Overwrite GD locale files if they exist, without asking.
    :type overwrite: bool

    :rtype: None
    """

    if verbose:
        print "Parsing entries in \"{}\"".format(infile)

    po = polib.pofile(infile)

    try:
        if "X-gdgettext" not in po.metadata:
            raise Exception()
    except:
        raise Exception(
            "File \"{}\" hasn't been generated by gdgettext".format(infile)
        )

    # I could've done this using multiple levels of ``for`` but I wanted to
    # show my coding skills off.
    # This turns a string like "file4.ext:13 file2.ext:37" into a dict like
    # { "file4.ext": 13, "file2.ext": 37 }
    # Also it will throw later in the code if the format is incorrect, but then
    # it's the user's fault for providing an invalid file.
    line_counts = dict([
        (pair[0], int(pair[1])) for pair in
        (i.split(":") for i in po.metadata["X-gdgettext-Line-Counts"].split())
    ])

    file_entries = {}

    entry_id = -1
    entry_count = 0
    for entry in po:
        entry_id += 1

        if len(entry.occurrences) < 1:
            if verbose:
                print "Skipping entry #{} due to missing reference".format(
                    entry_count
                )

                continue

        if entry.occurrences[0][0] not in file_entries:
            file_entries[entry.occurrences[0][0]] = []

        file_entries[entry.occurrences[0][0]].append(entry)

        entry_count += 1

    if verbose:
        print "{} entries parsed".format(entry_count)

    for (filename_base, entries) in file_entries.iteritems():
        if not _has_enough_lines(entries, line_counts[filename_base]):
            if verbose:
                print (
                    "All {} entries of \"{}\" skipped due to one of the "
                    "entries being beyond the expected end of file"
                ).format(len(entries), filename_base)

            continue

        filename = os.path.join(outdir, filename_base)

        if not overwrite and os.path.isfile(filename):
            if verbose:
                print "File \"{}\" already exists".format(filename)

            if not _ask_overwrite(filename):
                continue

        with io.open(filename, encoding="utf8", mode="w+") as f:
            if filename_base[0:5] == "tags_":  # disregard references here
                for entry in entries:
                    f.write(u"{}\n".format(
                        entry.msgstr if entry.translated() else entry.msgid
                    ))
            else:
                entry_lines = []
                for entry in entries:
                    entry_lines.append(sorted(
                        [int(ref[1]) for ref in entry.occurrences]
                    ))

                for i in xrange(1, line_counts[filename_base] + 1):
                    for entry_id, entry in enumerate(entries):
                        if len(entry_lines[entry_id]) > 0 and \
                                i == entry_lines[entry_id][0]:
                            f.write(
                                entry.msgstr if entry.translated()
                                else entry.msgid
                            )
                            entry_lines[entry_id].pop(0)

                            break

                    f.write(u"\n")

        if verbose:
            print "{} entries ({} lines) written to \"{}\"".format(
                len(entries),
                line_counts[filename_base],
                filename_base
            )

    langdef_out = os.path.join(outdir, "language.def")
    with io.open(langdef_out, mode="w+", encoding="utf8") as f:
        f.write(u"\n".join((
            u"Language=",
            u"Author=",
            u"\n"
        )))

    print (
        "ATTENTION: Please make sure you edit \"{}\" accordingly before you "
        "submit your translation."
    ).format(langdef_out)

    if verbose:
        print "*sluuuurp* Done!"


def _ask_overwrite(filename):
    while True:
        try:
            overwrite = None
            while overwrite not in ("y", "n"):
                overwrite = raw_input(
                    "Overwrite \"{}\"? (y/n): ".format(filename)
                )
        except EOFError:
            continue
        else:
            return (overwrite == "y")


def _find_duplicate(entrylist, msgid):
    try:
        return itertools.ifilter(lambda x: x.msgid == msgid, entrylist).next()
    except StopIteration:
        return None


def _has_enough_lines(entries, line_count):
    for entry in entries:
        for ref in entry.occurrences:
            if int(ref[1]) > line_count:
                return False

    return True
