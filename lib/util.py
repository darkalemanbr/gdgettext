""" Miscelaneous utilities from external sources. """


class FakeSection(object):
    """ See http://stackoverflow.com/a/2819788 """

    def __init__(self, fp, secname="DEFAULT"):
        self.fp = fp
        self.secname = "[{}]\n".format(secname)

    def readline(self):
        if self.secname:
            try:
                return self.secname
            finally:
                self.secname = None
        else:
            return self.fp.readline()
