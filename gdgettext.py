#!/usr/bin/env python

# This Source Code Form is subject to the terms of the Mozilla Public License,
# v. 2.0. If a copy of the MPL was not distributed with this file, You can
# obtain one at http://mozilla.org/MPL/2.0/.

import os
import argparse
from lib import converters


VERSION = "0.31a"


def main():
    arg_parser = argparse.ArgumentParser(
        description="Converts GD locale files to GNU gettext and gettext back "
        "into GD locales.",
        version=VERSION
    )

    arg_parser.add_argument(
        "input",
        type=os.path.abspath,
        action="store",
        metavar="INPUT",
        help="Directory containing GD locales or a .po file."
    )

    arg_parser.add_argument(
        "-o", "--output",
        type=os.path.abspath,
        action="store",
        default=os.getcwd(),
        metavar="PATH",
        help="Directory to output converted file(s); default is CWD."
    )

    arg_parser.add_argument(
        "-p", "--verbose",
        action="store_true",
        help="Print detailed conversion progress."
    )

    arg_parser.add_argument(
        "-w", "--overwrite",
        action="store_true",
        help="Overwrite existing file(s) without asking."
    )

    arg_parser.add_argument(
        "-d", "--keep-duplicates",
        action="store_true",
        help="When converting GD to gettext, will keep all duplicated strings "
             "in a file; default behavior is to create a single entry from all"
             " the duplicates and store the occurences alongside it."
    )

    arg_parser.add_argument(
        "-c", "--no-autocontext",
        action="store_false",
        dest="autocontext",
        help="When converting GD to gettext, won't try to guess the entry "
        "context automatically."
    )

    args = arg_parser.parse_args()

    if os.path.isdir(args.input):
        langdef = os.path.join(args.input, "language.def")
        if not os.path.isfile(langdef):
            raise argparse.ArgumentError(
                "Couldn't find \"{}\"".format(langdef)
            )

        converters.gd_to_gettext(args.input,
                                 os.path.join(args.output, "grimdawn.pot"),
                                 args.verbose, args.overwrite,
                                 args.keep_duplicates, args.autocontext
                                 )
    elif os.path.isfile(args.input):
        converters.gettext_to_gd(args.input, args.output, args.verbose,
                                 args.overwrite
                                 )
    else:
        raise argparse.ArgumentError("Invalid input path")


if __name__ == "__main__":
    main()
